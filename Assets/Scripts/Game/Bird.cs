using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Bird : MonoBehaviour
{
    private float score = 0;
    [SerializeField] TextMeshProUGUI m_Object;
    [SerializeField] TextMeshProUGUI finalScore;
    [SerializeField] GameObject gameOver;
    [SerializeField] GameObject scoreBoard;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        score = score + 1;
        m_Object.text = score.ToString();
        finalScore.text = score.ToString();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Pipes")
        {
            gameOver.SetActive(true);
            scoreBoard.SetActive(false);
            Destroy(gameObject);
        }
    }
}
