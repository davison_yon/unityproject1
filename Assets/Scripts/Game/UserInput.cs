using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInput : MonoBehaviour
{
    public Rigidbody rb;
    private float maxSpeed = 2f;
    // Start is called before the first frame update
    void Start()
    {
        Physics.gravity = new Vector3(0, -1.5f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //maxSpeed = maxSpeed + .00001f;
        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                transform.position = new Vector3(transform.position.x, 
                    transform.position.y + 1f, transform.position.z);
            }
        }
    }
}
