using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipes : MonoBehaviour
{
    public float speed = 3;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(transform.right * -1 * speed * Time.deltaTime, Space.World);
        if (transform.position.x < -28)
        {
            transform.position = new Vector3(-11, transform.position.y, -4.8f);
        }
    }
}
