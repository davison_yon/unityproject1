using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Gems : MonoBehaviour
{
    public float speed = 3;
    public Manager _manager;
    public GameObject nextGem;
    public GameObject _self;
    public TextMeshProUGUI gemValue;
    public TextMeshProUGUI finalGemValue;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        gemValue.text = _manager.gemCount.ToString();
        finalGemValue.text = _manager.gemCount.ToString();
        transform.Translate(transform.right * -1 * speed * Time.deltaTime, Space.World);
        if (transform.position.x < -28)
        {
            transform.position = new Vector3(-11, transform.position.y, -4.8f);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            nextGem.SetActive(true);
            _manager.gemCount++;
            _self.SetActive(false);
        }
        if (collision.gameObject.tag == "Pipes")
        {
          //  transform.position = new Vector3(transform.position.x + 1.5f, transform.position.y, transform.position.z);
          //  Debug.Log("Gem collided with a pipe");
        }
    }
}
