using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewGame : MonoBehaviour
{
   public void ClickNewGameButton()
    {
        Debug.Log("It works!");
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    } 
}
